#include "notificationqueue.h"


NotificationQueue::NotificationQueue(int max)
{
	this->maxQue = max + 1;
	this->front = this->maxQue - 1;
	this->rear = this->maxQue - 1;
	this->notifications = new NotificationData[maxQue];

}

NotificationQueue::NotificationQueue()
{
	this->maxQue = 501;
	this->front = this->maxQue - 1;
	this->rear = this->maxQue - 1;
	this->notifications = new NotificationData[maxQue];
}

NotificationQueue::~NotificationQueue()
{
	delete[] this->notifications;
}

void NotificationQueue::MakeEmpty()
{
	this->front = this->maxQue - 1;
	this->rear  = this->maxQue - 1;
}

bool NotificationQueue::IsEmpty() const
{
	return (this->rear == this->front);
}

bool NotificationQueue::IsFull() const
{
	return ((this->rear + 1) % this->maxQue == this->front);
}

void NotificationQueue::Enqueue(NotificationData notificationData)
{
	if (this->IsFull())
		throw FullNotificationQueue();
	else
	{ 
		this->rear = (this->rear + 1) % this->maxQue;
		this->notifications[this->rear] = notificationData;
	}


}

void NotificationQueue::Dequeue(NotificationData& notificationData)
{
	if (IsEmpty())
		throw EmptyNotificationQueue();
	else
	{
		this->front = (this->front + 1) % this->maxQue;
		notificationData = this->notifications[this->front];
	}
}