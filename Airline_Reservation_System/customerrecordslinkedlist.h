#include <iostream>
#include <string.h>
#include <cstdlib>
#include "customer.h"
#include "customernodestruct.h"

#ifndef CUSTOMERRECORDSLINKEDLIST_H
#define CUSTOMERRECORDSLINKEDLIST_H

class CustomerRecordsLinkedList
{
public:
    CustomerRecordsLinkedList();
    ~CustomerRecordsLinkedList();

    bool isFull() const;
    void makeEmpty(); // Clears the list to an empty state
    int getLength() const;
    Customer getCustomer(int, bool&);
    Customer getCustomer2(string, bool&);
    bool addCustomer(Customer);
    void removeCustomer(int);
    void resetIterator();
    Customer getNextCustomer();


private:
    CustomerNode* listData;
    int length;
    CustomerNode* currentPos;

};

#endif

