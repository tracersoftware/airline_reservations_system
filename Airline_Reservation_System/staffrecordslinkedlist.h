#include <iostream>
#include <string.h>
#include <cstdlib>
#include "staff.h"
#include "staffnodestruct.h"


#ifndef STAFFRECORDSLINKEDLIST_H
#define STAFFRECORDSLINKEDLIST_H

class StaffRecordsLinkedList
{
public:
    StaffRecordsLinkedList();
    ~StaffRecordsLinkedList();

    bool isFull() const;
    void makeEmpty(); // Clears the list to an empty state
    int getLength() const;
    Staff getStaff(string, bool&);
    Staff getStaff2(string, bool&);
    bool addStaff(Staff);
    void removeStaff(string);
    void resetIterator();
    Staff getNextStaff();


private:
    Node* listData;
    int length;
    Node* currentPos;

};

#endif



