#include "flightinforecordsetmgr.h"

FlightInfoRecordSetMgr::FlightInfoRecordSetMgr(FlightInfoRecordsLinkedList* _flightinforecordslinkedlist,string _data_file_path)
{
    this->flightinforecordslinkedlist = _flightinforecordslinkedlist;
    this->data_file_path = _data_file_path;
}

FlightInfoRecordSetMgr::~FlightInfoRecordSetMgr()
{
    
}

FlightInfo FlightInfoRecordSetMgr::getFlightInfoByNo(string flight_no)
{
    FlightInfo flight_info;
    bool found = false;

    flight_info = this->flightinforecordslinkedlist->getFlightInfo(flight_no, found);

    if (found)
    {
        return flight_info;
    }
    else
    {
        throw NonExistingDataException();
    }
    
}

Json::Value FlightInfoRecordSetMgr::getFlightInfoByNoJSON(string flight_no)
{
    FlightInfo flight_info;
    bool found = false;

    flight_info = this->flightinforecordslinkedlist->getFlightInfo(flight_no, found);

    if (found)
    {
        Json::Value root;
        root["flight_no"] = flight_info.flight_no;
        root["route"] = flight_info.route;
        root["departure_date"] = flight_info.departure_date;
        root["departure_time"] = flight_info.departure_time;
        root["arrival_time"] = flight_info.arrival_time;
        root["price"] = flight_info.price;
        root["available_seat_count"] = flight_info.available_seat_count;
        root["reserved_seat_count"] = flight_info.reserved_seat_count;
        root["status"] = flight_info.status;

        return root;
    }
    else
    {
        throw NonExistingDataException();
    }
}

bool FlightInfoRecordSetMgr::addFlightInfo(FlightInfo flight_info)
{
    if(this->flightinforecordslinkedlist->addFlightInfo(flight_info))
        return true;
    else
        return false;
}

bool FlightInfoRecordSetMgr::editFlightInfo(FlightInfo flight_info)
{
    
    this->deleteFlightInfo(flight_info.flight_no);
    if (this->addFlightInfo(flight_info))
        return true;
    else
        return false;

}

void FlightInfoRecordSetMgr::deleteFlightInfo(string flight_no)
{
    this->flightinforecordslinkedlist->removeFlightInfo(flight_no);
}

Json::Value FlightInfoRecordSetMgr::getAllFlightInfo()
{
    this->flightinforecordslinkedlist->resetIterator();//Always reset iterator to head node

    int num_of_records = this->flightinforecordslinkedlist->getLength();
    Json::Value root;

    FlightInfo flight_info;

    for (int i = 0;i < num_of_records;i++)
    {
        Json::Value record;
        flight_info = this->flightinforecordslinkedlist->getNextFlightInfo();
        
        record["flight_no"] = flight_info.flight_no;
        record["route"] = flight_info.route;
        record["departure_date"] = flight_info.departure_date;
        record["departure_time"] = flight_info.departure_time;
        record["arrival_time"] = flight_info.arrival_time;
        record["price"] = flight_info.price;
        record["available_seat_count"] = flight_info.available_seat_count;
        record["reserved_seat_count"] = flight_info.reserved_seat_count;
        record["status"] = flight_info.status;
        
        root["flight_records"]["data"].append(record);
    }

    return root;

}

Json::Value FlightInfoRecordSetMgr::getAllFlightInfoBySearchParams(string route, string departure_date)
{
    this->flightinforecordslinkedlist->resetIterator();//Always reset iterator to head node

    int num_of_records = this->flightinforecordslinkedlist->getLength();
    Json::Value root;

    FlightInfo flight_info;

    for (int i = 0;i < num_of_records;i++)
    {
        Json::Value record;
        flight_info = this->flightinforecordslinkedlist->getNextFlightInfo();

        if ((flight_info.route == route) && (flight_info.departure_date == departure_date))
        {
            record["flight_no"] = flight_info.flight_no;
            record["route"] = flight_info.route;
            record["departure_date"] = flight_info.departure_date;
            record["departure_time"] = flight_info.departure_time;
            record["arrival_time"] = flight_info.arrival_time;
            record["price"] = flight_info.price;
            record["available_seat_count"] = flight_info.available_seat_count;
            record["reserved_seat_count"] = flight_info.reserved_seat_count;
            record["status"] = flight_info.status;

            root["flight_records"]["data"].append(record);
        }
    }

    return root;

}

void FlightInfoRecordSetMgr::populateFromFile()
{
    //open and read staff.json file
    ifstream flightinfo_records_file (this->data_file_path, ios::binary);

    Json::Value root;
    Json::Reader reader;

    if (flightinfo_records_file.is_open())
    {
        reader.parse(flightinfo_records_file, root);

        //get 'staff_records' entity from json file
        Json::Value flightinfo_records;
        flightinfo_records = root["flight_records"]["data"];

        for (Json::Value::ArrayIndex i = 0; i != flightinfo_records.size(); i++)
        {
            FlightInfo flight_info;
            flight_info.flight_no = (string)flightinfo_records[i]["flight_no"].asString();
            flight_info.route = (string)flightinfo_records[i]["route"].asString();
            flight_info.departure_date = (string)flightinfo_records[i]["departure_date"].asString();
            flight_info.departure_time = (string)flightinfo_records[i]["departure_time"].asString();
            flight_info.arrival_time = (string)flightinfo_records[i]["arrival_time"].asString();
            flight_info.price = (float)flightinfo_records[i]["price"].asFloat();
            flight_info.available_seat_count = (int)flightinfo_records[i]["available_seat_count"].asInt();
            flight_info.reserved_seat_count = (int)flightinfo_records[i]["reserved_seat_count"].asInt();
            flight_info.status = (flight_status)flightinfo_records[i]["status"].asInt();

            if (!this->addFlightInfo(flight_info)) //addCustomer fails due to memory shortage
            {
                throw ListFullException();
            }

        }

        //close file on-completion of reading
        flightinfo_records_file.close();

    }
    else
    {
        throw FileOpenException();
    }
}

void FlightInfoRecordSetMgr::saveToFile()
{
    //build json data for the file
    Json::Value root;

    root["file_information"]["file_description"] = "Flight Information Database";//header section of file

    Json::Value subroot; 
    subroot = this->getAllFlightInfo();
    root["flight_records"]["data"] = subroot["flight_records"]["data"];

    std::ofstream customer_records_file;
    customer_records_file.open(this->data_file_path, std::ofstream::out | std::ofstream::trunc);

    if (customer_records_file.is_open())
    {
        customer_records_file << root.toStyledString();
        customer_records_file.close();
        std::cout << "Wrote to file" << std::endl;
    }
    else
    {
       throw FileOpenException();
    }
    
}