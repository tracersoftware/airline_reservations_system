#include <iostream>
#include <string.h>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include "staff.h"
#include "staffrecordslinkedlist.h"
#include "json/json.h"
#include "NonExistingDataException.h"
#include "FileOpenException.h"
#include "ListFullException.h"

using namespace std;

#ifndef STAFFRECORDSETMGR_H
#define STAFFRECORDSETMGR_H

class StaffRecordSetMgr
{
public:
    StaffRecordSetMgr(StaffRecordsLinkedList *, string);
    ~StaffRecordSetMgr();

    Staff getStaffByID(string);
    Staff getStaffByUsername(string);
    Json::Value getStaffByIDJSON(string);

    bool addStaff(Staff);
    void deleteStaff(string);

    Json::Value getAllStaff();

    void populateFromFile();
    void saveToFile();

private:
    string data_file_path;
    StaffRecordsLinkedList* staffrecordslinkedlist;


};

#endif
