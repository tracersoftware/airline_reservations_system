#include <iostream>
#include <string>

using namespace std;

#ifndef NOTIFICATIONDATA_H
#define NOTIFICATIONDATA_H
class NotificationData
{
public:
    NotificationData();
    ~NotificationData();
    string email_address;
    string name;
    string subject;
    string content;

};

#endif;