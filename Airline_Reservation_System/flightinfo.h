#include <iostream>
#include <string.h>
#include <cstdlib>

#ifndef FLIGHTINFO_H
#define FLIGHTINFO_H

enum flight_status { AVAILABLE, CANCELLED };

using namespace std;

class FlightInfo
{
   public:
       string flight_no;
       string route; //e.g IAH-SFO
       string departure_date;
       string departure_time;
       string arrival_time;
       float price;
       int available_seat_count;
       int reserved_seat_count;
       flight_status status; 

       FlightInfo();
       FlightInfo(string, string, string, string, string, float, int, int, flight_status);
       ~FlightInfo();



};

#endif
