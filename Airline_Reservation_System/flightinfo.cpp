#include "flightinfo.h"

FlightInfo::FlightInfo()
{

}

FlightInfo::FlightInfo(string flight_no, string route, string departure_date, string departure_time, string arrival_time, float price, int available_seat_count, int reserved_seat_count, flight_status status)
{
	this->flight_no = flight_no;
	this->route = route;
	this->departure_date = departure_time;
	this->arrival_time = arrival_time;
	this->price = price;
	this->available_seat_count = available_seat_count;
	this->reserved_seat_count = reserved_seat_count;
	this->status = status;
}

FlightInfo::~FlightInfo()
{

}