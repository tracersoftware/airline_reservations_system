#include "reservation.h"

#ifndef RESERVATIONNODESTRUCT_H
#define RESERVATIONNODESTRUCT_H
struct ReservationNode;

struct ReservationNode
{
    Reservation reservationObj;
    ReservationNode* left;
    ReservationNode* right;
};
#endif
