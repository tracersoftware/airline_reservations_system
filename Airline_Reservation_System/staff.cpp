#include "staff.h"
#include "WrongPasswordException.h"

Staff::Staff()
{

}

Staff::Staff(string staff_id, string fullname, string username, string password, string designation)
{
	this->staff_id = staff_id;
	this->fullname = fullname;
	this->username = username;
	this->password = password;
	this->designation = designation;
}

Staff::~Staff()
{
	
}

void Staff::setStaffID(string staff_id) { this->staff_id = staff_id; }
string Staff::getStaffID() { return this->staff_id; }

void Staff::setFullName(string fullname) { this->fullname = fullname; }
string Staff::getFullName() { return this->fullname; }

void Staff::setUserName(string username) { this->username = username; }
string Staff::getUserName() { return this->username; }

void Staff::setPassword(string password) { this->password = password; }
string Staff::getPassword() { return this->password; }

void Staff::setDesignation(string designation) { this->designation = designation; }
string Staff::getDesignation() { return this->designation; }

bool Staff::changePassword(string old_password, string new_password)
{
	if (old_password == this->password)
	{
		this->password = new_password;
		return true;
	}
	else
	{
		throw new WrongPasswordException;
	}
}