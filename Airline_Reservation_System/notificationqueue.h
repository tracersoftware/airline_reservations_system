#include <iostream>
#include <string>
#include "notificationdata.h"



#ifndef NOTIFICATIONQUEUE_H
#define NOTIFICATIONQUEUE_H

class FullNotificationQueue
{};

class EmptyNotificationQueue
{};

using namespace std;

class NotificationQueue
{
     public: 
         NotificationQueue(int max);
         NotificationQueue();
         ~NotificationQueue();
         void MakeEmpty();
         bool IsEmpty() const;
         bool IsFull() const;
         void Enqueue(NotificationData);
         void Dequeue(NotificationData&);

     private:
         int front;
         int rear;
         NotificationData* notifications;
         int maxQue;

};

#endif; // !NOTIFICATIONQUEUE_H


