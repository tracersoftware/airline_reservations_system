#include <iostream>
#include <string.h>
#include <cstdlib>
#include "flightinfo.h"
#include "flightinfonodestruct.h"


#ifndef FLIGHTINFORECORDSLINKEDLIST_H
#define FLIGHTINFORECORDSLINKEDLIST_H

class FlightInfoRecordsLinkedList
{
public:
    FlightInfoRecordsLinkedList();
    ~FlightInfoRecordsLinkedList();

    bool isFull() const;
    void makeEmpty(); // Clears the list to an empty state
    int getLength() const;
    FlightInfo getFlightInfo(string, bool&);
    bool addFlightInfo(FlightInfo);
    void removeFlightInfo(string);
    void resetIterator();
    FlightInfo getNextFlightInfo();


private:
    FlightInfoNode* listData;
    int length;
    FlightInfoNode* currentPos;

};

#endif



