#include <iostream>
#include <string.h>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include "customer.h"
#include "customerrecordslinkedlist.h"
#include "json/json.h"
#include "NonExistingDataException.h"
#include "FileOpenException.h"
#include "ListFullException.h"

using namespace std;

#ifndef CUSTOMERRECORDSETMGR_H
#define CUSTOMERRECORDSETMGR_H

class CustomerRecordSetMgr
{
public:
    CustomerRecordSetMgr(CustomerRecordsLinkedList*, string);
    ~CustomerRecordSetMgr();

    Customer getCustomerByID(int);
    Customer getCustomerByEmail(string);
    Json::Value getCustomerByIDJSON(int);

    bool addCustomer(Customer);
    void deleteCustomer(int);

    Json::Value getAllCustomers();

    void populateFromFile();
    void saveToFile();

private:
    string data_file_path;
    CustomerRecordsLinkedList* customrecordslinkedlist;


};

#endif
