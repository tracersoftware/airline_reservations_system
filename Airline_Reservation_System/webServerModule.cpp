#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTMLForm.h"
#include "Poco/Net/PartHandler.h"
#include "Poco/Net/MessageHeader.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/CountingStream.h"
#include "Poco/NullStream.h"
#include "Poco/StreamCopier.h"
#include "Poco/Exception.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Thread.h"
#include "Poco/Runnable.h"
#include <iostream>
#include "json/json.h"
#include "controller.h"
#include "NonExistingDataException.h"


using Poco::Net::ServerSocket;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::Net::MessageHeader;
using Poco::Net::HTMLForm;
using Poco::Net::NameValueCollection;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;
using Poco::CountingInputStream;
using Poco::NullOutputStream;
using Poco::StreamCopier;

class RootHandler : public Poco::Net::HTTPRequestHandler
{
public:
	void handleRequest(Poco::Net::HTTPServerRequest& request,
		Poco::Net::HTTPServerResponse& response)
	{
		Application& app = Application::instance();
		app.logger().information("Request from " +
			request.clientAddress().toString());
		response.setChunkedTransferEncoding(true);
		response.setContentType("text/html");
		std::ostream& ostr = response.send();
		ostr << "<html><head><title>Airline Reservation System 1.0< / title> < / head>";
		ostr << "<body>";

		ostr << "</body></html>";
	}
};

class MainRequestHandler : public HTTPRequestHandler
{

public:
	MainRequestHandler(Controller* _controller)
	{
		this->controller = _controller;
	}

	void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response)
	{
		Json::Value root;
		Json::Reader reader;

		Application& app = Application::instance();
		app.logger().information("Request from " + request.clientAddress().toString());

		HTMLForm form(request, request.stream());
		response.setChunkedTransferEncoding(true);
		response.setContentType("application/json");

		std::ostream& ostr = response.send();

		if (request.getURI() == "/customerlogin")
		{
			
				reader.parse(form["request_data"], root);
				Json::Value response_data = this->controller->customerLogin(root["email"].asString(), root["password"].asString());

				ostr << response_data.toStyledString();		
		}
		else if (request.getURI() == "/getcustomer")
		{
			     reader.parse(form["request_data"], root);
				 Json::Value response_data = this->controller->getCustomerProfile(root["_id"].asInt());

				 ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/getallcustomers")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->getAllCustomers();

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/addcustomer")
		{
			Json::Value response_data = this->controller->addCustomer(form["request_data"]);

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/deletecustomer")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->deleteCustomer(root["_id"].asInt());

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/addflight")
		{
			Json::Value response_data = this->controller->addFlightInformation(form["request_data"]);

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/getallflights")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->getAllFlights();

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/searchforflights")
		{
			     reader.parse(form["request_data"], root);
				 Json::Value response_data = this->controller->searchForFlights(root["route"].asString(), root["departure_date"].asString());

				 ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/getflightinfo")
		{
			     reader.parse(form["request_data"], root);
				 Json::Value response_data = this->controller->getFlightInformation(root["flight_no"].asString());

				 ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/deleteflightinfo")
		{
				 reader.parse(form["request_data"], root);
				 Json::Value response_data = this->controller->deleteFlightInfo(root["flight_no"].asString());

				 ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/searchforreservation")
		{
				reader.parse(form["request_data"], root);
				Json::Value response_data = this->controller->getReservation(root["reservation_code"].asInt());

				ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/getallreservations")
		{
			    reader.parse(form["request_data"], root);
				Json::Value response_data = this->controller->getAllReservations();

				ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/addreservation")
		{
			Json::Value response_data = this->controller->addReservation(form["request_data"]);

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/cancelreservation")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->cancelReservation(root["reservation_code"].asInt());

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/stafflogin")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->staffLogin(root["username"].asString(), root["password"].asString());

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/addstaff")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->addStaff(form["request_data"]);

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/getstaff")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->getStaffProfile(root["staff_id"].asString());

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/getallstaff")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->getAllStaff();

			ostr << response_data.toStyledString();
		}
		else if (request.getURI() == "/deletestaff")
		{
			reader.parse(form["request_data"], root);
			Json::Value response_data = this->controller->deleteStaff(root["staff_id"].asString());

			ostr << response_data.toStyledString();
		}
		else
		{
			Json::Value response_data = this->controller->unknownMethod();
			ostr << response_data.toStyledString();
		}

		/*
		Json::Value root;
		Json::Reader reader;

		reader.parse(form["mydata"], root);

		std::cout << "Transaction Type: " << root["transtype"] << std::endl;
		std::cout << "Status Type: " << root["Status"] << std::endl; */

		

		


	}

private:
	Controller* controller;
};

class MyRequestHandlerFactory : public
	Poco::Net::HTTPRequestHandlerFactory
{
public:
	MyRequestHandlerFactory(Controller* _controller)
	{
		this->controller = _controller;
	}
	Poco::Net::HTTPRequestHandler* createRequestHandler(
		const Poco::Net::HTTPServerRequest& request)
	{
		if (request.getURI() == "/")
			return new RootHandler();
		else
			return new MainRequestHandler(this->controller);
	}

private:
	Controller* controller;
};


class HTTPFormServer : public Poco::Util::ServerApplication
{
	/*
	  The main HTTP Server Application class
	*/

public:
	
	HTTPFormServer(Controller* _controller)
	{
		this->controller = _controller;
	}

	~HTTPFormServer()
	{
	}

	int main(const std::vector<std::string>& args)
	{
		Poco::UInt16 port = 9980;
		HTTPServerParams* pParams = new HTTPServerParams;
		pParams->setMaxQueued(100);
		pParams->setMaxThreads(16);
		ServerSocket svs(port); // set-up a server socket
		HTTPServer srv(new MyRequestHandlerFactory(this->controller), svs, pParams);

		// start the HTTPServer
		srv.start();
		waitForTerminationRequest();
		// Stop the HTTPServer
		srv.stop();
		return Application::EXIT_OK;
	}

private:
	Controller* controller;

};
