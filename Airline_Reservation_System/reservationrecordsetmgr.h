#include <iostream>
#include <string.h>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include "flightinfo.h"
#include "flightinforecordslinkedlist.h"
#include "json/json.h"
#include "NonExistingDataException.h"
#include "reservationbinarysearchtree.h"
#include "FileOpenException.h"
#include "ListFullException.h"

#ifndef RESERVATIONRECORDSETMGR_H
#define RESERVATIONRECORDSETMGR_H

using namespace std;

class ReservationRecordSetMgr
{
public:
    ReservationRecordSetMgr(ReservationRecordsBST*, string);
    ~ReservationRecordSetMgr();

    Reservation getReservationByCode(int);

    bool addReservation(Reservation);
    void deleteReservation(int);

    Json::Value getAllReservations();

    void populateFromFile();
    void saveToFile();

private:
    string data_file_path;
    ReservationRecordsBST* reservationrecordsBST;
};

#endif;