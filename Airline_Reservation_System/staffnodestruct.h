//#pragma once
#include "staff.h"
#ifndef STAFFNODESTRUCT_H
#define STAFFNODESTRUCT_H
struct Node;

struct Node
{
    Staff staffObj;
    Node* next;
};

#endif
