#include <iostream>
#include <string.h>
#include <cstdlib>
#include "reservation.h"
#include "reservationnodestruct.h"
#include "QueType.h"

#ifndef RESERVATIONRECORDSBST_H
#define RESERVATIONRECORDSBST_H

enum OrderType { PRE_ORDER, IN_ORDER, POST_ORDER };
class ReservationRecordsBST
{
public:
	ReservationRecordsBST();                     // constructor
	~ReservationRecordsBST();                    // destructor
	ReservationRecordsBST(const ReservationRecordsBST& originalTree);
	void operator=(const ReservationRecordsBST& originalTree);
	// copy constructor
	void MakeEmpty();
	bool IsEmpty() const;
	bool IsFull() const;
	int GetLength() const;
	Reservation GetReservation(Reservation item, bool& found);
	void PutReservation(Reservation item);
	void DeleteReservation(Reservation item);
	void ResetTree(OrderType order);
	Reservation GetNextReservation(OrderType order, bool& finished);
	void Print(std::ofstream& outFile) const;
private:
	ReservationNode* root;
	QueType preQue;
	QueType inQue;
	QueType postQue;
};

#endif;
