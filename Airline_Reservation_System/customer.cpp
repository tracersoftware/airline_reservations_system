#include "customer.h"
#include "WrongPasswordException.h"

Customer::Customer()
{

}

Customer::Customer(int _id, string firstname, string lastname, string phone_number, string email, string password, string address)
{
	this->_id = _id;
	this->firstname = firstname;
	this->lastname = lastname;
	this->phone_number = phone_number;
	this->email = email;
	this->password = password;
	this->address = address;
}

Customer::~Customer()
{

}

void Customer::setID(int _id) { this->_id = _id; }
int Customer::getID() { return this->_id; }

void Customer::setFirstname(string firstname) { this->firstname = firstname; }
string Customer::getFirstname() { return this->firstname; };

void Customer::setLastname(string lastname) { this->lastname = lastname; }
string Customer::getLastname() { return this->lastname; }

void Customer::setPhoneNumber(string phone_number) { this->phone_number = phone_number; }
string Customer::getPhoneNumber() { return this->phone_number; }

void Customer::setEmail(string email) { this->email = email; }
string Customer::getEmail() { return this->email; }

void Customer::setPassword(string password) { this->password = password; }
string Customer::getPassword() { return this->password; }

void Customer::setAddress(string address) { this->address = address; }
string Customer::getAddress() { return this->address; }


bool Customer::changePassword(string old_password, string new_password)
{
	if (old_password == this->password)
	{
		this->password = new_password;
		return true;
	}
	else
	{
		throw new WrongPasswordException;
	}
}