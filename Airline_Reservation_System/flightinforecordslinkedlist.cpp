#include "flightinforecordslinkedlist.h"


FlightInfoRecordsLinkedList::FlightInfoRecordsLinkedList()
{
    this->length = 0;
    this->listData = NULL;
}

FlightInfoRecordsLinkedList::~FlightInfoRecordsLinkedList()
{
    FlightInfoNode* temp;

    while (this->listData != NULL)
    {
        temp = this->listData;
        this->listData = this->listData->next;
        delete temp;
    }
}

bool FlightInfoRecordsLinkedList::isFull() const
{
    FlightInfoNode* curr_location;

    try
    {
        curr_location = new FlightInfoNode;
        delete curr_location;
        return false;
    }
    catch (std::bad_alloc exception)
    {
        return true;
    }
}

void FlightInfoRecordsLinkedList::makeEmpty()
{
    FlightInfoNode* temp;

    while (this->listData != NULL)
    {
        temp = this->listData;
        this->listData = this->listData->next;
        delete temp;
    }

    this->length = 0;
}

int FlightInfoRecordsLinkedList::getLength() const
{
    return this->length;
}

FlightInfo FlightInfoRecordsLinkedList::getFlightInfo(string flight_no, bool &found)
{
    FlightInfo flightinfo_data;

    bool moreToSearch;
    FlightInfoNode* curr_location;

    curr_location = this->listData;
    found = false;
    moreToSearch = (curr_location != NULL);

    while (moreToSearch && !found)
    {
        if (curr_location->flightinfoObj.flight_no == flight_no)
        {
            found = true;
            flightinfo_data = curr_location->flightinfoObj;
            break;
        }
        else
        {
            curr_location = curr_location->next;
            moreToSearch = (curr_location != NULL);

        }
    }

    return flightinfo_data;
}

bool FlightInfoRecordsLinkedList::addFlightInfo(FlightInfo flightinfo)
{
    if (!this->isFull())
    {
        FlightInfoNode* curr_location;

        curr_location = new FlightInfoNode;
        curr_location->flightinfoObj = flightinfo;
        curr_location->next = this->listData;

        this->listData = curr_location;

        this->length++;

        return true;
    }
    else
        return false;

}

void FlightInfoRecordsLinkedList::removeFlightInfo(string flight_no)
{
    FlightInfoNode* curr_location = this->listData;
    FlightInfoNode* temp;

    if (curr_location->flightinfoObj.flight_no == flight_no)
    {
        temp = curr_location;
        this->listData = this->listData->next;
    }
    else
    {
        while ((curr_location->next)->flightinfoObj.flight_no != flight_no)
            curr_location = curr_location->next;

        temp = curr_location->next;//this points to the location of the required node
        curr_location->next = (curr_location->next)->next;
    }

    delete temp;
    this->length--;

}

void FlightInfoRecordsLinkedList::resetIterator()
{
    this->currentPos = NULL;
}

FlightInfo FlightInfoRecordsLinkedList::getNextFlightInfo()
{
    FlightInfo flightinfo_data;



    if (this->currentPos == NULL)
    {
        this->currentPos = this->listData;
    }
    else
    {
        this->currentPos = this->currentPos->next;
    }

    flightinfo_data = this->currentPos->flightinfoObj;
    return flightinfo_data;


}





