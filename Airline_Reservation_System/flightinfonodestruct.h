#include "flightinfo.h"

#ifndef FLIGHTINFONODESTRUCT_H
#define FLIGHTINFONODESTRUCT_H

struct FlightInfoNode;

struct FlightInfoNode
{
    FlightInfo flightinfoObj;
    FlightInfoNode* next;
};

#endif
