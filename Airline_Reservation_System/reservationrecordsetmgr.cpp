#include "reservationrecordsetmgr.h"

ReservationRecordSetMgr::ReservationRecordSetMgr(ReservationRecordsBST* _reservationrecordsBST, string _data_file_path)
{
	this->reservationrecordsBST = _reservationrecordsBST;
	this->data_file_path = _data_file_path;
}

ReservationRecordSetMgr::~ReservationRecordSetMgr()
{

}

Reservation ReservationRecordSetMgr::getReservationByCode(int reservation_code)
{
	Reservation reservation, reservation_search_obj;
	bool found = false;

	reservation_search_obj.reservation_code = reservation_code;//assign a Reservation object's reservation_code attribute with search variable
    
	reservation = this->reservationrecordsBST->GetReservation(reservation_search_obj, found);

	if (found)
	{
		return reservation;
	}
	else
	{
		throw NonExistingDataException();
	}

}

bool ReservationRecordSetMgr::addReservation(Reservation reservation)
{
	try
	{
		this->reservationrecordsBST->PutReservation(reservation);
		return true;
	}
	catch (exception e)
	{
		return false;
	}
}

void ReservationRecordSetMgr::deleteReservation(int reservation_code)
{
	Reservation reservation;
	reservation.reservation_code = reservation_code;

	this->reservationrecordsBST->DeleteReservation(reservation);
}

Json::Value ReservationRecordSetMgr::getAllReservations()
{
    this->reservationrecordsBST->ResetTree(IN_ORDER);

    Json::Value root;

    bool finished = false;

    Reservation reservation;
    

    while (!finished)
    {
        reservation = this->reservationrecordsBST->GetNextReservation(IN_ORDER, finished);
        Json::Value record;

        record["flight_departure_date"] = reservation.flight_departure_date;
        record["reservation_code"] = reservation.reservation_code;
        record["flight_no"] = reservation.flight_no;
        record["flight_route"] = reservation.flight_route;
        record["flight_departure_time"] = reservation.flight_departure_time;
        record["flight_arrival_time"] = reservation.flight_arrival_time;
        record["passenger_name"] = reservation.passenger_name;
        record["passenger_email"] = reservation.passenger_email;
        record["price"] = reservation.price;

        root["reservation_records"]["data"].append(record);
    }

	return root;

}

void ReservationRecordSetMgr::populateFromFile()
{
    //open and read staff.json file
    ifstream reservation_records_file(this->data_file_path, ios::binary);

    Json::Value root;
    Json::Reader reader;

    if (reservation_records_file.is_open())
    {
        reader.parse(reservation_records_file, root);

        //get 'staff_records' entity from json file
        Json::Value reservation_records;
        reservation_records = root["reservation_records"]["data"];

        for (Json::Value::ArrayIndex i = 0; i != reservation_records.size(); i++)
        {
            Reservation reservation;
            reservation.flight_departure_date = reservation_records[i]["flight_departure_date"].asString();
            reservation.reservation_code = reservation_records[i]["reservation_code"].asInt();
            reservation.flight_no = reservation_records[i]["flight_no"].asString();
            reservation.flight_route = reservation_records[i]["flight_route"].asString();
            reservation.flight_departure_time = reservation_records[i]["flight_departure_time"].asString();
            reservation.flight_arrival_time = reservation_records[i]["flight_arrival_time"].asString();
            reservation.passenger_name = reservation_records[i]["passenger_name"].asString();
            reservation.passenger_email = reservation_records[i]["passenger_email"].asString();
            reservation.price = reservation_records[i]["price"].asFloat();

            if (!this->addReservation(reservation)) //addCustomer fails due to memory shortage
            {
                throw ListFullException();
            }

        }

        //close file on-completion of reading
        reservation_records_file.close();

    }
    else
    {
        throw FileOpenException();
    }
}

void ReservationRecordSetMgr::saveToFile()
{
    //build json data for the file
    Json::Value root;

    root["file_information"]["file_description"] = "Reservations Database";//header section of file

    Json::Value subroot;
    subroot = this->getAllReservations();
    root["reservation_records"]["data"] = subroot["reservation_records"]["data"];

    std::ofstream reservation_records_file;
    reservation_records_file.open(this->data_file_path, std::ofstream::out | std::ofstream::trunc);

    if (reservation_records_file.is_open())
    {
        reservation_records_file << root.toStyledString();
        reservation_records_file.close();
        std::cout << "Wrote to file" << std::endl;
    }
    else
    {
        throw FileOpenException();
    }
}