//#pragma once
#include "customer.h"

#ifndef CUSTOMERNODESTRUCT_H
#define CUSTOMERNODESTRUCT_H
struct CustomerNode;

struct CustomerNode
{
    Customer customerObj;
    CustomerNode* next;
};
#endif
