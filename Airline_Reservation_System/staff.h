#include <iostream>
#include <string.h>
#include <cstdlib>

#ifndef STAFF_H
#define STAFF_H



using namespace std;

class Staff
{
private:
    string staff_id;
    string fullname;
    string username;
    string password;
    string designation;

public:
    Staff();
    Staff(string, string, string, string, string);
    ~Staff();

    void setStaffID(string);
    string getStaffID();

    void setFullName(string);
    string getFullName();

    void setUserName(string);
    string getUserName();

    void setPassword(string);
    string getPassword();

    void setDesignation(string);
    string getDesignation();

    bool changePassword(string, string);

};

#endif // !STAFF_H