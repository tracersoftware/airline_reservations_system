#include <cstdlib>
#include <iostream>
#include <string>
#include "Poco/Net/SMTPClientSession.h"
#include "Poco/Net/MailMessage.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTMLForm.h"


using namespace Poco::Net;
using namespace std;

#ifndef MAILMGR_H
#define MAILMGR_H

class MailMgr
{
	public:
		MailMgr();
		~MailMgr();
		void sendMail(string, string, string, string);

    private:
		/*
		string acct_username;
		string acct_password;
		string smtp_server;
		int port;
		*/

};

#endif;
