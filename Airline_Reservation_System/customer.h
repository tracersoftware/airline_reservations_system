
#include <iostream>
#include <string.h>
#include <cstdlib>

#ifndef CUSTOMER_H
#define CUSTOMER_H



using namespace std;

class Customer
{
   public:
	   Customer();
       Customer(int _id, string firstname, string lastname, string phone_number, string email, string password, string address);
	   ~Customer();

	   void setID(int);
	   int getID();

	   void setFirstname(string);
	   string getFirstname();

	   void setLastname(string);
	   string getLastname();

	   void setPhoneNumber(string);
	   string getPhoneNumber();

	   void setEmail(string);
	   string getEmail();

	   void setPassword(string);
	   string getPassword();

	   void setAddress(string);
	   string getAddress();

       bool changePassword(string, string);
	  

   private:
		int _id;
		string firstname;
		string lastname;
		string phone_number;
		string email;
		string address;
		string password;


};

#endif // !CUSTOMER_H
