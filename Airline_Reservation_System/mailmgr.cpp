#include "mailmgr.h"
#include <new>

MailMgr::MailMgr()
{
	
}

MailMgr::~MailMgr()
{

}

void MailMgr::sendMail(string receiver, string name, string subject, string content)
{
	HTTPClientSession session("127.0.0.1",8081);
	session.setTimeout(Poco::Timespan(20, 0));

	HTTPRequest request(HTTPRequest::HTTP_POST, "/ars_mailer_engine/index.php", HTTPMessage::HTTP_1_1);
	HTMLForm form;

	form.add("recipient_email", receiver);
	form.add("recipient_name", name);
	form.add("email_subject", subject);
	form.add("email_content", content);
	form.prepareSubmit(request);
	form.write(session.sendRequest(request));
	//session.sendRequest(request);

	HTTPResponse response;
	std::istream& rs = session.receiveResponse(response);

	string output;

	while (rs >> output)
		std::cout << output;

}