#include "controller.h"

using namespace std;

Controller::Controller(StaffRecordSetMgr* _staffRecordSetMgr, CustomerRecordSetMgr* _customerRecordSetMgr, FlightInfoRecordSetMgr* _flightInfoRecordSetMgr, ReservationRecordSetMgr* _reservationRecordSetMgr, NotificationQueue* _notificationQueue)
{
	this->staffRecordSetMgr = _staffRecordSetMgr;
	this->customerRecordSetMgr = _customerRecordSetMgr;
	this->flightInfoRecordSetMgr = _flightInfoRecordSetMgr;
	this->reservationRecordSetMgr = _reservationRecordSetMgr;
	this->notificationQueue = _notificationQueue;
}

Controller::~Controller()
{

}

Json::Value Controller::customerLogin(string email, string password)
{
	try 
	{
		Json::Value root;

		Customer customer;
		customer = this->customerRecordSetMgr->getCustomerByEmail(email);

		if ((customer.getPassword() == password) && (password != "-"))
		{
			Json::Value data;

			data["_id"] = customer.getID();
			data["firstname"] = customer.getFirstname();
			data["lastname"] = customer.getLastname();
			data["phone_number"] = customer.getPhoneNumber();
			data["email"] = customer.getEmail();
			data["address"] = customer.getAddress();
			data["password"] = customer.getPassword();

			root["function_status"] = "SUCCESS";
			root["msg"] = "";
			root["data"] = data;

			
		}
		else
		{
			root["function_status"] = "FAIL";
			root["msg"] = "";

		}

		return root;
	}
	catch(NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Account Not Found!";

		return root_e;
	}
}

Json::Value Controller::addCustomer(string request_data)
{
	Json::Value root;

	try
	{
		Json::Value data;
		Json::Reader reader;

		reader.parse(request_data,data);

		Customer customer;
		customer.setID(this->generateCustomerID());
		customer.setFirstname(data["firstname"].asString());
		customer.setLastname(data["lastname"].asString());
		customer.setPhoneNumber(data["phone_number"].asString());
		customer.setAddress(data["address"].asString());
		customer.setEmail(data["email"].asString());
		customer.setPassword(data["password"].asString());

		if (this->customerRecordSetMgr->addCustomer(customer))
		{
			root["function_status"] = "SUCCESS";
			root["msg"] = "";
			root["data"] = "";
		}
		else
		{
			root["function_status"] = "FAIL";
			root["msg"] = "Application Error!";
			root["data"] = "";
		}

		return root;

	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error Occurred, please contact the Administrator!";

		return root_e;
	}
}

bool Controller::checkIfCustomerIDExists(int _id)
{
	try 
	{
		if (this->customerRecordSetMgr->getCustomerByID(_id).getID() == _id)
		{
			return true;
		}
	}
	catch (NonExistingDataException)
	{
		return false;
	}
}

int Controller::generateCustomerID()
{
	int customer_id = 100000 + (rand() % 400001);

	if (!this->checkIfCustomerIDExists(customer_id))
	{
		return customer_id;
	}
	else
	{
		customer_id = this->generateCustomerID();
		return customer_id;
	}

}

Json::Value Controller::getCustomerProfile(int _id)
{
	Json::Value root;

	try
	{
		Customer customer;
		customer = this->customerRecordSetMgr->getCustomerByID(_id);

		Json::Value data;

		data["_id"] = customer.getID();
		data["firstname"] = customer.getFirstname();
		data["lastname"] = customer.getLastname();
		data["phone_number"] = customer.getPhoneNumber();
		data["email"] = customer.getEmail();
		data["address"] = customer.getAddress();
		data["password"] = customer.getPassword();

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = data;

		return root;


	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Account Not Found!";

		return root_e;
	}

}

Json::Value Controller::getAllCustomers()
{
	Json::Value root;

	try
	{
		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = this->customerRecordSetMgr->getAllCustomers();

		return root;
	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error!";

		return root_e;
	}
}

Json::Value Controller::deleteCustomer(int _id)
{
	Json::Value root;

	if (this->checkIfCustomerIDExists(_id))
	{
		this->customerRecordSetMgr->deleteCustomer(_id);

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = "";
	}
	else
	{
		root["function_status"] = "FAIL";
		root["msg"] = "Customer Information does not exist";
		root["data"] = "";
	}

	return root;
}

//Staff Info

Json::Value Controller::addStaff(string request_data)
{
	Json::Value root;

	try
	{
		Json::Value data;
		Json::Reader reader;

		reader.parse(request_data, data);

		Staff staff;
		staff.setStaffID(data["staff_id"].asString());
		staff.setFullName(data["fullname"].asString());
		staff.setUserName(data["username"].asString());
		staff.setPassword(data["password"].asString());
		staff.setDesignation(data["designation"].asString());

		if (this->checkIfStaffIDExists(staff.getStaffID()))
			throw NonExistingDataException();

		if (this->staffRecordSetMgr->addStaff(staff))
		{
			root["function_status"] = "SUCCESS";
			root["msg"] = "";
			root["data"] = "";
		}
		else
		{
			root["function_status"] = "FAIL";
			root["msg"] = "Application Error!";
			root["data"] = "";
		}

		return root;

	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error Occurred, please contact the Administrator!";

		return root_e;
	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Staff ID exists!";

		return root_e;
	}
}

Json::Value Controller::staffLogin(string username, string password)

{
	try
	{
		Json::Value root;

		Staff staff;
		staff = this->staffRecordSetMgr->getStaffByUsername(username);

		if ((staff.getPassword() == password) && (password != "-"))
		{
			Json::Value data;

			data["staff_id"] = staff.getStaffID();
			data["username"] = staff.getUserName();
			data["password"] = "******";
			data["fullname"] = staff.getFullName();
			data["designation"] = staff.getDesignation();

			root["function_status"] = "SUCCESS";
			root["msg"] = "";
			root["data"] = data;


		}
		else
		{
			root["function_status"] = "FAIL";
			root["msg"] = "";

		}

		return root;
	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Account Not Found!";

		return root_e;
	}
}

Json::Value Controller::getStaffProfile(string staff_id)
{
	Json::Value root;

	try
	{
		Staff staff;
		staff = this->staffRecordSetMgr->getStaffByID(staff_id);

		Json::Value data;

		data["staff_id"] = staff.getStaffID();
		data["username"] = staff.getUserName();
		data["password"] = staff.getPassword();
		data["fullname"] = staff.getFullName();
		data["designation"] = staff.getDesignation();

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = data;

		return root;


	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Account Not Found!";

		return root_e;
	}

}

Json::Value Controller::getAllStaff()
{
	Json::Value root;

	try
	{
		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = this->staffRecordSetMgr->getAllStaff();

		return root;
	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error!";

		return root_e;
	}
}

Json::Value Controller::deleteStaff(string staff_id)
{
	Json::Value root;

	if (this->checkIfStaffIDExists(staff_id))
	{
		this->staffRecordSetMgr->deleteStaff(staff_id);

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = "";
	}
	else
	{
		root["function_status"] = "FAIL";
		root["msg"] = "Staff Information does not exist";
		root["data"] = "";
	}

	return root;
}

bool Controller::checkIfStaffIDExists(string staff_id)
{
	try
	{
		if (this->staffRecordSetMgr->getStaffByID(staff_id).getStaffID() == staff_id)
		{
			return true;
		}
	}
	catch (NonExistingDataException)
	{
		return false;
	}
}

//Flight Info

Json::Value Controller::addFlightInformation(string request_data)
{
	Json::Value root;

	try
	{
		Json::Value data;
		Json::Reader reader;

		reader.parse(request_data, data);

		if (this->checkIfFlightInfoExists(data["flight_no"].asString()))//if flight no already exists, throw DuplicateDataException
		{
			throw DuplicateDataException();
		}
		else
		{
			FlightInfo flightinfo;

			flightinfo.flight_no = data["flight_no"].asString();
			flightinfo.route     = data["route"].asString();
			flightinfo.departure_date = data["departure_date"].asString();
			flightinfo.departure_time = data["departure_time"].asString();
			flightinfo.arrival_time = data["arrival_time"].asString();
			flightinfo.price = data["price"].asFloat();
			flightinfo.available_seat_count = data["available_seat_count"].asInt();
			flightinfo.reserved_seat_count = data["reserved_seat_count"].asInt();
			flightinfo.status = (flight_status)data["status"].asInt();

			if (this->flightInfoRecordSetMgr->addFlightInfo(flightinfo))
			{
				root["function_status"] = "SUCCESS";
				root["msg"] = "";
				root["data"] = "";
			}
			else
			{
				root["function_status"] = "FAIL";
				root["msg"] = "Application Error!";
				root["data"] = "";
			}

			return root;

		}
		

	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error Occurred, please contact the Administrator!";

		return root_e;
	}
	catch (DuplicateDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Flight No. already exists, Try again!";

		return root_e;
	}

}

Json::Value Controller::getAllFlights()
{
	Json::Value root;

	try
	{
		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = this->flightInfoRecordSetMgr->getAllFlightInfo();

		return root;
	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error!";

		return root_e;
	}
}

Json::Value Controller::searchForFlights(string route, string departure_date)
{
	Json::Value root;

	try
	{
		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = this->flightInfoRecordSetMgr->getAllFlightInfoBySearchParams(route, departure_date);

		return root;

	}
	catch(exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error!";

		return root_e;
	}
	
}

Json::Value Controller::getFlightInformation(string flight_no)
{
	Json::Value root;

	try
	{
		Json::Value data;

		FlightInfo flightInfo;

		flightInfo = this->flightInfoRecordSetMgr->getFlightInfoByNo(flight_no);

		data["flight_no"] = flightInfo.flight_no;
		data["route"] = flightInfo.route;
		data["departure_date"] = flightInfo.departure_date;
		data["departure_time"] = flightInfo.departure_time;
		data["arrival_time"] = flightInfo.arrival_time;
		data["price"] = flightInfo.price;
		data["available_seat_count"] = flightInfo.available_seat_count;
		data["reserved_seat_count"] = flightInfo.reserved_seat_count;
		data["status"] = flightInfo.status;

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = data;

		return root;

	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Flight Information not found!";

		return root_e;
	}

	
}

Json::Value Controller::deleteFlightInfo(string flight_no)
{
	Json::Value root;

	if (this->checkIfFlightInfoExists(flight_no))
	{
		this->flightInfoRecordSetMgr->deleteFlightInfo(flight_no);

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = "";
	}
	else
	{
		root["function_status"] = "FAIL";
		root["msg"] = "Flight Information does not exist";
		root["data"] = "";
	}

	return root;

}

bool Controller::checkIfFlightInfoExists(string flight_no)
{
	try 
	{
		if (this->flightInfoRecordSetMgr->getFlightInfoByNo(flight_no).flight_no == flight_no)
		{
			return true;
		}
	}
	catch (NonExistingDataException)
	{
		return false;
	}

}

Json::Value Controller::addReservation(string request_data)
{
	Json::Value root;

	try
	{
		Json::Value data;
		Json::Reader reader;

		reader.parse(request_data, data);

		Reservation reservation;
		reservation.reservation_code = this->generateReservationCode();
		reservation.price = data["price"].asFloat();
		reservation.passenger_name = data["passenger_name"].asString();
		reservation.passenger_email = data["passenger_email"].asString();
		reservation.flight_route = data["flight_route"].asString();
		reservation.flight_no = data["flight_no"].asString();
		reservation.flight_departure_date = data["flight_departure_date"].asString();
		reservation.flight_departure_time = data["flight_departure_time"].asString();
		reservation.flight_arrival_time = data["flight_arrival_time"].asString();

		if (this->reservationRecordSetMgr->addReservation(reservation))
		{
			this->updateFlightInformationOnReservation(reservation.flight_no, 'B');
			int reservation_code = reservation.reservation_code;
			root["function_status"] = "SUCCESS";
			root["msg"] = "";
			root["data"]["reservation_code"] = to_string(reservation_code);

			//log reservation notification to notifications queuing system
			NotificationData notificationData;
			notificationData.email_address = reservation.passenger_email;
			notificationData.name = reservation.passenger_name;
			notificationData.subject = "Flight Reservation Notification";
			notificationData.content = "<p>Flight Reservation for: " + reservation.passenger_name + "</p>" +
				"<p>Flight Number: " + reservation.flight_no + "</p>" +
				"<p>Route: " + reservation.flight_route + "</p>" +
				"<p>Date: " + reservation.flight_departure_date + "</p>" +
				"<p>Departure: " + reservation.flight_departure_time + "</p>" +
				"<p>Arrival: " + reservation.flight_arrival_time + "</p>" +
				"<p>Price: $" + to_string(reservation.price) + "</p>" +
				"<p>Thank you for flying with us</p>";

			this->notificationQueue->Enqueue(notificationData);


		}
		else
		{
			root["function_status"] = "FAIL";
			root["msg"] = "Application Error!";
			root["data"] = "";
		}

		return root;

	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error Occurred, please contact the Administrator!";

		return root_e;
	}
}

bool Controller::checkIfReservationCodeExists(int reservation_code)
{
	try
	{
		if (this->reservationRecordSetMgr->getReservationByCode(reservation_code).reservation_code == reservation_code)
		{
			return true;
		}
	}
	catch (NonExistingDataException)
	{
		return false;
	}
}

int Controller::generateReservationCode()
{
	int reservation_code = 100000 + (rand() % 400001);

	if (!this->checkIfReservationCodeExists(reservation_code))
	{
		return reservation_code;
	}
	else
	{
		reservation_code = this->generateReservationCode();
		return reservation_code;
	}

}

void Controller::updateFlightInformationOnReservation(string flight_no,char operation)
{
	try
	{
	 
		FlightInfo flightinfo;
		flightinfo = this->flightInfoRecordSetMgr->getFlightInfoByNo(flight_no);

		if (operation == 'B')// 'B' booking flight
			flightinfo.reserved_seat_count = flightinfo.reserved_seat_count + 1;
		else // 'C' cancelling flight
			flightinfo.reserved_seat_count = flightinfo.reserved_seat_count - 1;

		//delete flight information and re-add updated version
		this->flightInfoRecordSetMgr->deleteFlightInfo(flight_no);
		this->flightInfoRecordSetMgr->addFlightInfo(flightinfo);


	}
	catch (exception e)
	{
		throw e;
	}
	catch (NonExistingDataException e)
	{
		throw e;
	}
	
}

Json::Value Controller::getReservation(int reservation_code)
{
	Json::Value root;
    
	try
	{
		Json::Value data;

		Reservation reservation;

		reservation = this->reservationRecordSetMgr->getReservationByCode(reservation_code);
        
		data["flight_departure_date"] = reservation.flight_departure_date;
		data["reservation_code"] = reservation.reservation_code;
		data["flight_no"] = reservation.flight_no;
		data["flight_route"] = reservation.flight_route;
		data["flight_departure_time"] = reservation.flight_departure_time;
		data["flight_arrival_time"] = reservation.flight_arrival_time;
		data["passenger_name"] = reservation.passenger_name;
		data["passenger_email"] = reservation.passenger_email;
		data["price"] = reservation.price;

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = data;

		return root;


	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Reservation not found!";

		return root_e;
	}
}

Json::Value Controller::getAllReservations()
{
	Json::Value root;
	try
	{
		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = this->reservationRecordSetMgr->getAllReservations();

		return root;
	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error!";

		return root_e;
	}
	
}

Json::Value Controller::cancelReservation(int reservation_code)
{
	try
	{
		Json::Value root;

		//get flight information from reservation data
		Reservation reservation;
		reservation = this->reservationRecordSetMgr->getReservationByCode(reservation_code);

		//delete reservation
		this->reservationRecordSetMgr->deleteReservation(reservation_code);
        
		//update flight information
		this->updateFlightInformationOnReservation(reservation.flight_no, 'C');

		root["function_status"] = "SUCCESS";
		root["msg"] = "";
		root["data"] = "";

		return root;

	}
	catch (NonExistingDataException e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Reservation or Flight Information not found!";

		return root_e;
	}
	catch (exception e)
	{
		Json::Value root_e;
		root_e["function_status"] = "EXCEPTION";
		root_e["msg"] = "Server Error!";

		return root_e;
	}


}

Json::Value Controller::unknownMethod()
{
	Json::Value root_e;
	root_e["function_status"] = "EXCEPTION";
	root_e["msg"] = "Method Unknown, try again!";

	return root_e;
}