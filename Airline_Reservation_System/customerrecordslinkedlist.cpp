#include "customerrecordslinkedlist.h"


CustomerRecordsLinkedList::CustomerRecordsLinkedList()
{
    this->length = 0;
    this->listData = NULL;
}

CustomerRecordsLinkedList::~CustomerRecordsLinkedList()
{
    CustomerNode* temp;

    while (this->listData != NULL)
    {
        temp = this->listData;
        this->listData = this->listData->next;
        delete temp;
    }
}

bool CustomerRecordsLinkedList::isFull() const
{
    CustomerNode* curr_location;

    try
    {
        curr_location = new CustomerNode;
        delete curr_location;
        return false;
    }
    catch (std::bad_alloc exception)
    {
        return true;
    }
}

void CustomerRecordsLinkedList::makeEmpty()
{
    CustomerNode* temp;

    while (this->listData != NULL)
    {
        temp = this->listData;
        this->listData = this->listData->next;
        delete temp;
    }

    this->length = 0;
}

int CustomerRecordsLinkedList::getLength() const
{
    return this->length;
}

Customer CustomerRecordsLinkedList::getCustomer(int _id, bool& found)
{
    Customer customer_data((int)0,"-","-","-","-","-","-");

    bool moreToSearch;
    CustomerNode* curr_location;

    curr_location = this->listData;
    found = false;
    moreToSearch = (curr_location != NULL);

    while (moreToSearch && !found)
    {
        if (curr_location->customerObj.getID() == _id)
        {
            found = true;
            customer_data = curr_location->customerObj;
            break;
        }
        else
        {
            curr_location = curr_location->next;
            moreToSearch = (curr_location != NULL);

        }
    }

    return customer_data;
}

Customer CustomerRecordsLinkedList::getCustomer2(string email, bool& found)
{
    Customer customer_data((int)0, "-", "-", "-", "-", "-", "-");

    bool moreToSearch;
    CustomerNode* curr_location;

    curr_location = this->listData;
    found = false;
    moreToSearch = (curr_location != NULL);

    while (moreToSearch && !found)
    {
        if (curr_location->customerObj.getEmail() == email)
        {
            found = true;
            customer_data = curr_location->customerObj;
            break;
        }
        else
        {
            curr_location = curr_location->next;
            moreToSearch = (curr_location != NULL);

        }
    }

    return customer_data;
}

bool CustomerRecordsLinkedList::addCustomer(Customer customer)
{
    if (!this->isFull())
    {
        CustomerNode* curr_location;

        curr_location = new CustomerNode;
        curr_location->customerObj = customer;
        curr_location->next = this->listData;

        this->listData = curr_location;

        this->length++;

        return true;
    }
    else
        return false;

}

void CustomerRecordsLinkedList::removeCustomer(int _id)
{
    CustomerNode* curr_location = this->listData;
    CustomerNode* temp;

    if (curr_location->customerObj.getID() == _id)
    {
        temp = curr_location;
        this->listData = this->listData->next;
    }
    else
    {
        while ((curr_location->next)->customerObj.getID() != _id)
            curr_location = curr_location->next;

        temp = curr_location->next;//this points to the location of the required node
        curr_location->next = (curr_location->next)->next;
    }

    delete temp;
    this->length--;

}

void CustomerRecordsLinkedList::resetIterator()
{
    this->currentPos = NULL;
}

Customer CustomerRecordsLinkedList::getNextCustomer()
{
    Customer customer_data;



    if (this->currentPos == NULL)
    {
        this->currentPos = this->listData;
    }
    else
    {
        this->currentPos = this->currentPos->next;
    }

    customer_data = this->currentPos->customerObj;
    return customer_data;


}





