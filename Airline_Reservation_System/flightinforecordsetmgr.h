#include <iostream>
#include <string.h>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include "flightinfo.h"
#include "flightinforecordslinkedlist.h"
#include "json/json.h"
#include "NonExistingDataException.h"
#include "FileOpenException.h"
#include "ListFullException.h"

using namespace std;

#ifndef FLIGHTINFORECORDSETMGR_H
#define FLIGHTINFORECORDSETMGR_H

class FlightInfoRecordSetMgr
{
public:
    FlightInfoRecordSetMgr(FlightInfoRecordsLinkedList*, string);
    ~FlightInfoRecordSetMgr();

    FlightInfo getFlightInfoByNo(string);
    Json::Value getFlightInfoByNoJSON(string);

    bool addFlightInfo(FlightInfo);
    bool editFlightInfo(FlightInfo);
    void deleteFlightInfo(string);

    Json::Value getAllFlightInfo();
    Json::Value getAllFlightInfoBySearchParams(string, string);

    void populateFromFile();
    void saveToFile();

private:
    string data_file_path;
    FlightInfoRecordsLinkedList* flightinforecordslinkedlist;


};

#endif
