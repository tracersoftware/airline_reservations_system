#include "customerrecordsetmgr.h"

CustomerRecordSetMgr::CustomerRecordSetMgr(CustomerRecordsLinkedList*_customerrecordslinkedlist,string _data_file_path)
{
    this->customrecordslinkedlist = _customerrecordslinkedlist;
    this->data_file_path = _data_file_path;
}

CustomerRecordSetMgr::~CustomerRecordSetMgr()
{
    
}

Customer CustomerRecordSetMgr::getCustomerByID(int id)
{
    Customer customer_data;
    bool found = false;

    customer_data = this->customrecordslinkedlist->getCustomer(id, found);

    if (found)
    {
        return customer_data;
    }
    else
    {
        throw NonExistingDataException();
    }
    
}

Customer CustomerRecordSetMgr::getCustomerByEmail(string email)
{
    Customer customer_data;
    bool found = false;

    customer_data = this->customrecordslinkedlist->getCustomer2(email, found);

    if (found)
    {
        return customer_data;
    }
    else
    {
        throw NonExistingDataException();
    }
}

Json::Value CustomerRecordSetMgr::getCustomerByIDJSON(int id)
{
    Customer customer;
    bool found = false;

    customer = this->customrecordslinkedlist->getCustomer(id, found);

    if (found)
    {
        Json::Value root;
        root["_id"] = customer.getID();
        root["firstname"] = customer.getFirstname();
        root["lastname"] = customer.getLastname();
        root["phone_number"] = customer.getPhoneNumber();
        root["email"] = customer.getEmail();
        root["address"] = customer.getAddress();
        root["password"] = customer.getPassword();

        return root;
    }
    else
    {
        throw NonExistingDataException();
    }
}

bool CustomerRecordSetMgr::addCustomer(Customer customer_data)
{
    if(this->customrecordslinkedlist->addCustomer(customer_data))
        return true;
    else
        return false;
}

void CustomerRecordSetMgr::deleteCustomer(int id)
{
    this->customrecordslinkedlist->removeCustomer(id);
}

Json::Value CustomerRecordSetMgr::getAllCustomers()
{
    this->customrecordslinkedlist->resetIterator();//Always reset iterator to head node

    int num_of_records = this->customrecordslinkedlist->getLength();
    Json::Value root;

    Customer customer;

    for (int i = 0;i < num_of_records;i++)
    {
        Json::Value record;
        customer = this->customrecordslinkedlist->getNextCustomer();
        
        record["_id"] = customer.getID();
        record["firstname"] = customer.getFirstname();
        record["lastname"] = customer.getLastname();
        record["phone_number"] = customer.getPhoneNumber();
        record["email"] = customer.getEmail();
        record["address"] = customer.getAddress();
        record["password"] = customer.getPassword();
        
        root["customer_records"]["data"].append(record);
    }

    return root;

}

void CustomerRecordSetMgr::populateFromFile()
{
    //open and read staff.json file
    ifstream customer_records_file (this->data_file_path, ios::binary);

    Json::Value root;
    Json::Reader reader;

    if (customer_records_file.is_open())
    {
        reader.parse(customer_records_file, root);

        //get 'staff_records' entity from json file
        Json::Value customer_records;
        customer_records = root["customer_records"]["data"];

        for (Json::Value::ArrayIndex i = 0; i != customer_records.size(); i++)
        {
            Customer customer;
            customer.setID((int)customer_records[i]["_id"].asInt());
            customer.setFirstname((string)customer_records[i]["firstname"].asString());
            customer.setLastname((string)customer_records[i]["lastname"].asString());
            customer.setPhoneNumber((string)customer_records[i]["phone_number"].asString());
            customer.setEmail((string)customer_records[i]["email"].asString());
            customer.setAddress((string)customer_records[i]["address"].asString());
            customer.setPassword((string)customer_records[i]["password"].asString());

            if (!this->addCustomer(customer)) //addCustomer fails due to memory shortage
            {
                throw new ListFullException;
            }

        }

        //close file on-completion of reading
        customer_records_file.close();

    }
    else
    {
        throw FileOpenException();
    }
}

void CustomerRecordSetMgr::saveToFile()
{
    //build json data for the file
    Json::Value root;

    root["file_information"]["file_description"] = "Customers Database";//header section of file

    Json::Value subroot; 
    subroot = this->getAllCustomers();
    root["customer_records"]["data"] = subroot["customer_records"]["data"];

    std::ofstream customer_records_file;
    customer_records_file.open(this->data_file_path, std::ofstream::out | std::ofstream::trunc);
   // fstream staff_records_file(this->data_file_path, std::ofstream::in | std::ofstream::out | std::ofstream::app | std::ofstream::trunc);

    if (customer_records_file.is_open())
    {
        customer_records_file << root.toStyledString();
        customer_records_file.close();
        std::cout << "Wrote to file" << std::endl;
    }
    else
    {
       throw FileOpenException();
    }
    
}