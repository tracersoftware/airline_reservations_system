#include "staffrecordslinkedlist.h"


StaffRecordsLinkedList::StaffRecordsLinkedList()
{
    this->length = 0;
    this->listData = NULL;
}

StaffRecordsLinkedList::~StaffRecordsLinkedList()
{
    Node* temp;

    while (this->listData != NULL)
    {
        temp = this->listData;
        this->listData = this->listData->next;
        delete temp;
    }
}

bool StaffRecordsLinkedList::isFull() const
{
    Node* curr_location;

    try
    {
        curr_location = new Node;
        delete curr_location;
        return false;
    }
    catch (std::bad_alloc exception)
    {
        return true;
    }
}

void StaffRecordsLinkedList::makeEmpty()
{
    Node* temp;

    while (this->listData != NULL)
    {
        temp = this->listData;
        this->listData = this->listData->next;
        delete temp;
    }

    this->length = 0;
}

int StaffRecordsLinkedList::getLength() const
{
    return this->length;
}

Staff StaffRecordsLinkedList::getStaff(string staff_id, bool &found)
{
    Staff staff_data;

    bool moreToSearch;
    Node* curr_location;

    curr_location = this->listData;
    found = false;
    moreToSearch = (curr_location != NULL);

    while (moreToSearch && !found)
    {
        if (curr_location->staffObj.getStaffID() == staff_id)
        {
            found = true;
            staff_data = curr_location->staffObj;
            break;
        }
        else
        {
            curr_location = curr_location->next;
            moreToSearch = (curr_location != NULL);

        }
    }

    return staff_data;
}

Staff StaffRecordsLinkedList::getStaff2(string username, bool& found)
{
    Staff staff_data;

    bool moreToSearch;
    Node* curr_location;

    curr_location = this->listData;
    found = false;
    moreToSearch = (curr_location != NULL);

    while (moreToSearch && !found)
    {
        if (curr_location->staffObj.getUserName() == username)
        {
            found = true;
            staff_data = curr_location->staffObj;
            break;
        }
        else
        {
            curr_location = curr_location->next;
            moreToSearch = (curr_location != NULL);

        }
    }

    return staff_data;
}

bool StaffRecordsLinkedList::addStaff(Staff staff)
{
    if (!this->isFull())
    {
        Node* curr_location;

        curr_location = new Node;
        curr_location->staffObj = staff;
        curr_location->next = this->listData;

        this->listData = curr_location;

        this->length++;

        return true;
    }
    else
        return false;

}

void StaffRecordsLinkedList::removeStaff(string staff_id)
{
    Node* curr_location = this->listData;
    Node* temp;

    if (curr_location->staffObj.getStaffID() == staff_id)
    {
        temp = curr_location;
        this->listData = this->listData->next;
    }
    else
    {
        while ((curr_location->next)->staffObj.getStaffID() != staff_id)
            curr_location = curr_location->next;

        temp = curr_location->next;//this points to the location of the required node
        curr_location->next = (curr_location->next)->next;
    }

    delete temp;
    this->length--;

}

void StaffRecordsLinkedList::resetIterator()
{
    this->currentPos = NULL;
}

Staff StaffRecordsLinkedList::getNextStaff()
{
    Staff staff_data;



    if (this->currentPos == NULL)
    {
        this->currentPos = this->listData;
    }
    else
    {
        this->currentPos = this->currentPos->next;
    }

    staff_data = this->currentPos->staffObj;
    return staff_data;


}





