#include "reservationbinarysearchtree.h"

bool ReservationRecordsBST::IsFull() const
// Returns true if there is no room for another item 
//  on the free store; false otherwise.
{
    ReservationNode* location;
    try
    {
        location = new ReservationNode;
        delete location;
        return false;
    }
    catch (std::bad_alloc exception)
    {
        return true;
    }
}

bool ReservationRecordsBST::IsEmpty() const
// Returns true if the tree is empty; false otherwise.
{
    return root == NULL;
}

int CountNodes(ReservationNode* tree);

int ReservationRecordsBST::GetLength() const
// Calls recursive function CountNodes to count the 
// nodes in the tree.
{
    return CountNodes(root);
}


int CountNodes(ReservationNode* tree)
// Post: returns the number of nodes in the tree.
{
    if (tree == NULL)
        return 0;
    else
        return CountNodes(tree->left) + CountNodes(tree->right) + 1;
}

void Retrieve(ReservationNode* tree, Reservation& item, bool& found);

Reservation ReservationRecordsBST::GetReservation(Reservation item, bool& found)
// Calls recursive function Retrieve to search the tree for item.
{
    Retrieve(this->root, item, found);
    return item;
}


void Retrieve(ReservationNode* tree,
    Reservation& item, bool& found)
    // Recursively searches tree for item.
    // Post: If there is an element someItem whose key matches item's,
    //       found is true and item is set to a copy of someItem; 
    //       otherwise found is false and item is unchanged.
{
    if (tree == NULL)
        found = false;                     // item is not found.
    else if (item.reservation_code < tree->reservationObj.reservation_code)
        Retrieve(tree->left, item, found); // Search left subtree.
    else if (item.reservation_code > tree->reservationObj.reservation_code)
        Retrieve(tree->right, item, found);// Search right subtree.
    else
    {
        item = tree->reservationObj;                 // item is found.
        found = true;
    }
}

void Insert(ReservationNode*& tree, Reservation item);

void ReservationRecordsBST::PutReservation(Reservation item)
// Calls recursive function Insert to insert item into tree.
{
    Insert(root, item);
}


void Insert(ReservationNode*& tree, Reservation item)
// Inserts item into tree.
// Post:  item is in tree; search property is maintained.
{
    if (tree == NULL)
    {// Insertion place found.
        tree = new ReservationNode;
        tree->right = NULL;
        tree->left = NULL;
        tree->reservationObj = item;
    }
    else if (item.reservation_code < tree->reservationObj.reservation_code)
        Insert(tree->left, item);    // Insert in left subtree.
    else
        Insert(tree->right, item);   // Insert in right subtree.
}

void DeleteNode(ReservationNode*& tree);

void Delete(ReservationNode*& tree, Reservation item);

void ReservationRecordsBST::DeleteReservation(Reservation item)
// Calls recursive function Delete to delete item from tree.
{
    Delete(root, item);
}


void Delete(ReservationNode*& tree, Reservation item)
// Deletes item from tree.
// Post:  item is not in tree.
{
    if (item.reservation_code < tree->reservationObj.reservation_code)
        Delete(tree->left, item);   // Look in left subtree.
    else if (item.reservation_code > tree->reservationObj.reservation_code)
        Delete(tree->right, item);  // Look in right subtree.
    else
        DeleteNode(tree);           // Node found; call DeleteNode.
}

void GetPredecessor(ReservationNode* tree, Reservation& data);

void DeleteNode(ReservationNode*& tree)
// Deletes the node pointed to by tree.
// Post: The user's data in the node pointed to by tree is no 
//       longer in the tree.  If tree is a leaf node or has only 
//       non-NULL child pointer the node pointed to by tree is 
//       deleted; otherwise, the user's data is replaced by its 
//       logical predecessor and the predecessor's node is deleted.
{
    Reservation data;
    ReservationNode* tempPtr;

    tempPtr = tree;
    if (tree->left == NULL)
    {
        tree = tree->right;
        delete tempPtr;
    }
    else if (tree->right == NULL)
    {
        tree = tree->left;
        delete tempPtr;
    }
    else
    {
        GetPredecessor(tree->left, data);
        tree->reservationObj = data;
        Delete(tree->left, data);  // Delete predecessor node.
    }
}

void GetPredecessor(ReservationNode* tree, Reservation& data)
// Sets data to the info member of the right-most node in tree.
{
    while (tree->right != NULL)
        tree = tree->right;
    data = tree->reservationObj;
}

void PrintTree(ReservationNode* tree, std::ofstream& outFile)
// Prints info member of items in tree in sorted order on outFile.
{
    if (tree != NULL)
    {
        PrintTree(tree->left, outFile);   // Print left subtree.
       // outFile << tree->info;
        PrintTree(tree->right, outFile);  // Print right subtree.
    }
}

void ReservationRecordsBST::Print(std::ofstream& outFile) const
// Calls recursive function Print to print items in the tree.
{
    PrintTree(root, outFile);
}

ReservationRecordsBST::ReservationRecordsBST()
{
    root = NULL;
}

void Destroy(ReservationNode*& tree);

ReservationRecordsBST::~ReservationRecordsBST()
// Calls recursive function Destroy to destroy the tree.
{
    Destroy(root);
}


void Destroy(ReservationNode*& tree)
// Post: tree is empty; nodes have been deallocated.
{
    if (tree != NULL)
    {
        Destroy(tree->left);
        Destroy(tree->right);
        delete tree;
    }
}

void ReservationRecordsBST::MakeEmpty()
{
    Destroy(root);
    root = NULL;
}


void CopyTree(ReservationNode*& copy,
    const ReservationNode* originalTree);

ReservationRecordsBST::ReservationRecordsBST(const ReservationRecordsBST& originalTree)
// Calls recursive function CopyTree to copy originalTree 
//  into root.
{
    CopyTree(root, originalTree.root);
}

void ReservationRecordsBST::operator=
(const ReservationRecordsBST& originalTree)
// Calls recursive function CopyTree to copy originalTree 
// into root.
{
    {
        if (&originalTree == this)
            return;             // Ignore assigning self to self
        Destroy(root);      // Deallocate existing tree nodes
        CopyTree(root, originalTree.root);
    }

}
void CopyTree(ReservationNode*& copy,
    const ReservationNode* originalTree)
    // Post: copy is the root of a tree that is a duplicate 
    //       of originalTree.
{
    if (originalTree == NULL)
        copy = NULL;
    else
    {
        copy = new ReservationNode;
        copy->reservationObj = originalTree->reservationObj;
        CopyTree(copy->left, originalTree->left);
        CopyTree(copy->right, originalTree->right);
    }
}
// Function prototypes for auxiliary functions.

void PreOrder(ReservationNode*, QueType&);
// Enqueues tree items in preorder.


void InOrder(ReservationNode*, QueType&);
// Enqueues tree items in inorder.


void PostOrder(ReservationNode*, QueType&);
// Enqueues tree items in postorder.


void ReservationRecordsBST::ResetTree(OrderType order)
// Calls function to create a queue of the tree elements in 
// the desired order.
{
    switch (order)
    {
    case PRE_ORDER: PreOrder(root, preQue);
        break;
    case IN_ORDER: InOrder(root, inQue);
        break;
    case POST_ORDER: PostOrder(root, postQue);
        break;
    }
}


void PreOrder(ReservationNode* tree,
    QueType& preQue)
    // Post: preQue contains the tree items in preorder.
{
    if (tree != NULL)
    {
        preQue.Enqueue(tree->reservationObj);
        PreOrder(tree->left, preQue);
        PreOrder(tree->right, preQue);
    }
}


void InOrder(ReservationNode* tree,
    QueType& inQue)
    // Post: inQue contains the tree items in inorder.
{
    if (tree != NULL)
    {
        InOrder(tree->left, inQue);
        inQue.Enqueue(tree->reservationObj);
        InOrder(tree->right, inQue);
    }
}


void PostOrder(ReservationNode* tree,
    QueType& postQue)
    // Post: postQue contains the tree items in postorder.
{
    if (tree != NULL)
    {
        PostOrder(tree->left, postQue);
        PostOrder(tree->right, postQue);
        postQue.Enqueue(tree->reservationObj);
    }
}


Reservation ReservationRecordsBST::GetNextReservation(OrderType order, bool& finished)
// Returns the next item in the desired order.
// Post: For the desired order, item is the next item in the queue.
//       If item is the last one in the queue, finished is true; 
//       otherwise finished is false.
{
    finished = false;
    Reservation item;
    switch (order)
    {
    case PRE_ORDER: preQue.Dequeue(item);
        if (preQue.IsEmpty())
            finished = true;
        break;
    case IN_ORDER: inQue.Dequeue(item);
        if (inQue.IsEmpty())
            finished = true;
        break;
    case  POST_ORDER: postQue.Dequeue(item);
        if (postQue.IsEmpty())
            finished = true;
        break;
    }
    return item;
}