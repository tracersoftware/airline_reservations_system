#include "staffrecordsetmgr.h"

StaffRecordSetMgr::StaffRecordSetMgr(StaffRecordsLinkedList *_staffrecordslinkedlist,string _data_file_path)
{
    this->staffrecordslinkedlist = _staffrecordslinkedlist;
    this->data_file_path = _data_file_path;
}

StaffRecordSetMgr::~StaffRecordSetMgr()
{
    //delete this->staffrecordslinkedlist;
}

Staff StaffRecordSetMgr::getStaffByID(string staff_id)
{
    Staff staff_data;
    bool found = false;

   staff_data = this->staffrecordslinkedlist->getStaff(staff_id, found);

    if (found)
    {
        return staff_data;
    }
    else
    {
        throw NonExistingDataException();
    }
    
}

Staff StaffRecordSetMgr::getStaffByUsername(string username)
{
    Staff staff_data;
    bool found = false;

    staff_data = this->staffrecordslinkedlist->getStaff2(username, found);

    if (found)
    {
        return staff_data;
    }
    else
    {
        throw NonExistingDataException();
    }
}

Json::Value StaffRecordSetMgr::getStaffByIDJSON(string staff_id)
{
    Staff staff_data;
    bool found = false;

    staff_data = this->staffrecordslinkedlist->getStaff(staff_id, found);

    if (found)
    {
        Json::Value root;
        root["staff_id"] = staff_data.getStaffID();
        root["fullname"] = staff_data.getFullName();
        root["username"] = staff_data.getUserName();
        root["password"] = staff_data.getPassword();
        root["designation"] = staff_data.getDesignation();

        return root;
    }
    else
    {
        throw NonExistingDataException();
    }
}

bool StaffRecordSetMgr::addStaff(Staff staff_data)
{
    if(this->staffrecordslinkedlist->addStaff(staff_data))
        return true;
    else
        return false;
}

void StaffRecordSetMgr::deleteStaff(string staff_id)
{
    this->staffrecordslinkedlist->removeStaff(staff_id);
}

Json::Value StaffRecordSetMgr::getAllStaff()
{
    this->staffrecordslinkedlist->resetIterator();//Always reset iterator to head node

    int num_of_records = this->staffrecordslinkedlist->getLength();
    Json::Value root;

    Staff staff;

    for (int i = 0;i < num_of_records;i++)
    {
        Json::Value record;
        staff = this->staffrecordslinkedlist->getNextStaff();
        
        record["staff_id"] = staff.getStaffID();
        record["fullname"] = staff.getFullName();
        record["username"] = staff.getUserName();
        record["password"] = staff.getPassword();
        record["designation"] = staff.getDesignation();
        
        root["staff_records"]["data"].append(record);
    }

    return root;

}

void StaffRecordSetMgr::populateFromFile()
{
    //open and read staff.json file
    ifstream staff_records_file (this->data_file_path, ios::binary);

    Json::Value root;
    Json::Reader reader;

    if (staff_records_file.is_open())
    {
        reader.parse(staff_records_file, root);

        //get 'staff_records' entity from json file
        Json::Value staff_records;
        staff_records = root["staff_records"]["data"];

        for (Json::Value::ArrayIndex i = 0; i != staff_records.size(); i++)
        {
            Staff staff;
            staff.setStaffID((string)staff_records[i]["staff_id"].asString());
            staff.setFullName((string)staff_records[i]["fullname"].asString());
            staff.setUserName((string)staff_records[i]["username"].asString());
            staff.setPassword((string)staff_records[i]["password"].asString());
            staff.setDesignation((string)staff_records[i]["designation"].asString());

            if (!this->addStaff(staff)) //addStaff fails due to memory shortage
            {
                throw ListFullException();
            }

        }

        //close file on-completion of reading
        staff_records_file.close();

    }
    else
    {
        throw FileOpenException();
    }
}

void StaffRecordSetMgr::saveToFile()
{
    //build json data for the file
    Json::Value root;

    root["file_information"]["file_description"] = "Staff Database";//header section of file

    Json::Value subroot; 
    subroot = this->getAllStaff();
    root["staff_records"]["data"] = subroot["staff_records"]["data"];

    std::ofstream staff_records_file;
    staff_records_file.open(this->data_file_path, std::ofstream::out | std::ofstream::trunc);
   // fstream staff_records_file(this->data_file_path, std::ofstream::in | std::ofstream::out | std::ofstream::app | std::ofstream::trunc);

    if (staff_records_file.is_open())
    {
        staff_records_file << root.toStyledString();
        staff_records_file.close();
        std::cout << "Wrote to file" << std::endl;
    }
    else
    {
       throw FileOpenException();
    }
    
}