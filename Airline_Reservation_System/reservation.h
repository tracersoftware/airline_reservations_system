#include <iostream>
#include <string.h>
#include <cstdlib>

#ifndef RESERVATION_H
#define RESERVATION_H



using namespace std;

class Reservation
{
    public:
        int reservation_code;
        string flight_no;
        string flight_route;
        string flight_departure_date;
        string flight_departure_time;
        string flight_arrival_time;
        string passenger_name;
        string passenger_email;
        float price;

        Reservation();
        ~Reservation();

       
};

#endif; // !RESERVATION_H


