#include <iostream>
#include <cstdlib>
#include <string>
#include "json/json.h"
#include "customerrecordsetmgr.h"
#include "flightinforecordsetmgr.h"
#include "staffrecordsetmgr.h"
#include "reservationrecordsetmgr.h"
#include "DuplicateDataException.h"
#include "notificationqueue.h"


#ifndef CONTROLLER_H
#define CONTROLLER_H

class Controller
{
  public:
	Controller(StaffRecordSetMgr*, CustomerRecordSetMgr*, FlightInfoRecordSetMgr*, ReservationRecordSetMgr*, NotificationQueue*);
	~Controller();

	//Customer
	Json::Value customerLogin(string email, string password);
	Json::Value addCustomer(string);
	Json::Value resetCustomerPassword();
	Json::Value getCustomerProfile(int _id);
	Json::Value getAllCustomers();
	Json::Value deleteCustomer(int _id);

	//Staff
	Json::Value staffLogin(string, string);
	Json::Value addStaff(string);
	Json::Value getStaffProfile(string);
	Json::Value getAllStaff();
	Json::Value deleteStaff(string);

	//Flight Info
	Json::Value addFlightInformation(string);
	Json::Value searchForFlights(string route, string departure_date);
	Json::Value getAllFlights();
	Json::Value getFlightInformation(string flight_no);
	void updateFlightInformationOnReservation(string,char);
	Json::Value deleteFlightInfo(string);

	//Reservation
	Json::Value addReservation(string);
	Json::Value getReservation(int);
	Json::Value getAllReservations();
	Json::Value cancelReservation(int);

	//Unknown method
	Json::Value unknownMethod();

 private:
	 StaffRecordSetMgr* staffRecordSetMgr;
	 CustomerRecordSetMgr* customerRecordSetMgr;
	 FlightInfoRecordSetMgr* flightInfoRecordSetMgr;
	 ReservationRecordSetMgr* reservationRecordSetMgr;
	 NotificationQueue* notificationQueue;

	 bool checkIfCustomerIDExists(int _id);
	 int generateCustomerID();
	 bool checkIfReservationCodeExists(int);
	 int generateReservationCode();
	 bool checkIfFlightInfoExists(string);
	 bool checkIfStaffIDExists(string);
};

#endif
