// Airline_Reservation_System.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
#include <thread>
#include <chrono>
#include <string.h>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include "webServerModule.cpp"
#include "staff.h"
#include "staffrecordslinkedlist.h"
#include "staffrecordsetmgr.h"
#include "customer.h"
#include "customerrecordslinkedlist.h"
#include "customerrecordsetmgr.h"
#include "json/json.h"
#include "NonExistingDataException.h"
#include "FileOpenException.h"
#include "ListFullException.h"
#include "controller.h"
#include "reservationrecordsetmgr.h"
#include "mailmgr.h"
#include "notificationqueue.h"

using namespace std;

StaffRecordsLinkedList sr_ll;
CustomerRecordsLinkedList cr_ll;
FlightInfoRecordsLinkedList fir_ll;
ReservationRecordsBST rr_BST;
CustomerRecordSetMgr _customerRecordSetMgr(&cr_ll, "./customers.json");
StaffRecordSetMgr _staffRecordSetMgr(&sr_ll, "./staff.json");
FlightInfoRecordSetMgr _flightInfoRecordSetMgr(&fir_ll,"./flights.json");
ReservationRecordSetMgr _reservationRecordSetMgr(&rr_BST, "./reservations.json");
NotificationQueue _notificationQueue;

class NotificationRunnable : public Poco::Runnable
{
public:
	NotificationQueue* _notificationQueue;
	NotificationRunnable(NotificationQueue* _notificationQueue_)
	{
		this->_notificationQueue = _notificationQueue_;
	}

	virtual void run()
	{
		MailMgr mailMgr;
		for (;;)
		{
			Sleep(5000);
			if (this->_notificationQueue->IsEmpty() == false)
			{
				NotificationData notificationData;
				this->_notificationQueue->Dequeue(notificationData);
				mailMgr.sendMail(notificationData.email_address, notificationData.subject, notificationData.subject, notificationData.content);

			}
			else
			{
				cout << "Non Notifications" << endl;
			}
		}
		
	}
};


class WebServerRunnable : public Poco::Runnable
{
	

public:
	Controller* controller_;

	WebServerRunnable(Controller* _controller)
	{
		this->controller_ = _controller;
	}

	virtual void run()
	{
		char x[50] = {};
		char* y = x;
	    
		HTTPFormServer app(this->controller_);

			app.run(1,&y);
	}
};



int main()
{
	//load data from storage files into data structures
	_customerRecordSetMgr.populateFromFile();
	_staffRecordSetMgr.populateFromFile();
	_flightInfoRecordSetMgr.populateFromFile();
	_reservationRecordSetMgr.populateFromFile();




	Controller controller(&_staffRecordSetMgr, &_customerRecordSetMgr, &_flightInfoRecordSetMgr, &_reservationRecordSetMgr, &_notificationQueue);
	NotificationRunnable n_runnable(&_notificationQueue);
	WebServerRunnable w_runnable(&controller);   

	Poco::Thread n_thread;
	Poco::Thread w_thread;

	n_thread.start(n_runnable);
	w_thread.start(w_runnable);

	n_thread.join();
	w_thread.join();

	return 0;
}

/*

int main(int argc, char** argv)
{
	//load data from storage files into data structures
	_customerRecordSetMgr.populateFromFile();
	_staffRecordSetMgr.populateFromFile();
	_flightInfoRecordSetMgr.populateFromFile();
	_reservationRecordSetMgr.populateFromFile();


	Controller controller(&_staffRecordSetMgr,&_customerRecordSetMgr,&_flightInfoRecordSetMgr,&_reservationRecordSetMgr);
	HTTPFormServer app(&controller);

	return app.run(argc, argv);  
}
*/










// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
